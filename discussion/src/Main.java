import com.zuitt.example.*;

public class Main {
    public static void main(String[] args) {

        //Object-Oriented Programming
        //OOP is a programming model that allows developers

        //OOP Concepts
        //Class
        //Four Pillars of OOP

        //System.out.println("Hello world!");

        Car myCar = new Car();
        myCar.drive();

        myCar.setName("HiAce");
        myCar.setBrand("Toyota");
        myCar.setYearOfMake(2025);

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ".");

        //Composition and Inheritance
        // - allows modelling an object that is a subset of another object
            //It defines "is a relationship"
        //Composition - allows modelling objects that are made up of other objects
            //both entities are dependent on each other
            //
        //example
            //A car is a vehicle - inheritance
            //A car has a driver - composition

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ". It is driven by " + myCar.getDriverName() + ".");

        myCar.setDriver("Dodong");

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ". It is driven by " + myCar.getDriverName() + ".");

        //2. Inheritance
        //can be defined as the process where one class acquire the properties and methods of another class

        Dog myPet = new Dog();
        myPet.setName("Bantay");
        myPet.setColor("Brown");
        myPet.speak();

        System.out.println("My dog is " + myPet.getName() + " and it is a " + myPet.getColor() + " " + myPet.getBreed() + "!");

        myPet.call();

        Person child = new Person();
        child.sleep();

        child.morningGreet();
        child.holidayGreet();
    }
}