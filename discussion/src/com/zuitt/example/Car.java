package com.zuitt.example;

public class Car {
    //Access Modifier
    //- these are used to restrict the scope of a class, constructor, variable, method, or data
    // Four Types of Access Modifiers
    //1. Default - no keyword (Accessibility is within the package)

    //2. Private -

    //3. Protected -

    //4. Public -

    //Class creation
        //Four parts of class creation
        //1. Properties - are characteristics of objects

    private String name;
    private String brand;
    private int yearOfMake;

    //Make a driver component of the car
    private Driver driver;

        //2. Constructors - used to create/instantiate an object
            //a. empty constructor
    public Car(){
        this.yearOfMake = 2000;

        //added driver
        this.driver = new Driver("Badong");
    }
        //

            //b. parameterized constructor - creates an object with arguments/parameters
    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver("Badong");

    }
        //3. Getters and Setters - get and set the values of each property of the object
            //a. getters - retrieve the value of an instantiated object
    public String getName(){
        return this.name;
    }
    public String getBrand(){
        return this.brand;
    }
    public int getYearOfMake(){
        return this.yearOfMake;
    }
            //b. setters -
    public String getDriverName(){
        return this.driver.getName();
    }
    public void setName(String name){
        this.name = name;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public void setYearOfMake(int yearOfMake){
        if(yearOfMake <= 2023){
            this.yearOfMake = yearOfMake;
        }
    }
    public void setDriver(String driver){
        this.driver.setName(driver);
    }

    public void drive(){
        System.out.println("The car is running. Lets go!");
    }
}
