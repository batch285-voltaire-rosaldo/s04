package com.zuitt.example;

public class Person implements Actions, Greetings{

    public void sleep(){
        System.out.println(".....(snore)");
    }

    public void run(){
        System.out.println("Running");
    }

    public void morningGreet(){
        System.out.println("Good morning everyone!");
    }
    public void holidayGreet(){
        System.out.println("Happy Labor Day!");
    }
}
