package com.zuitt.example;
//Child class of Animal class
//extends - is used to inherit the properties and methods of the parent class
public class Dog extends Animal {
    //properties
    private String breed;

    //constructor
    public Dog(){
        //super -direct access with the original constructor
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }
    public String getBreed(){
        return this.breed;
    }
    public void setBreed(String breed){
        this.breed = breed;
    }
    public void speak(){
        System.out.println("Aww Aww!");
    }
    public void call(){
        super.call();
        System.out.println("Hi, I'm " + this.getName() + ". I am a dog! (from Dog.java)");
    }
}
